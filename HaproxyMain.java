
// Creating a text File using FileWriter 
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
// Reading data from a file using FileReader 
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class HaproxyMain {
	Template data = new Template();

	static String configPath = "/etc/haproxy/";
    static String configExt = "cfg"; // cfg - production
    static String configName = "haproxy." + configExt;


	public static void main(String args[]) throws IOException, ParseException {
		System.out.println("meow");
		// options(args);
		// play();
		// File tempFile = new File(configName);
		// boolean exists = tempFile.exists();
		// // Create a new file if conf file does not exist.
		// if (!exists) {
		// 	System.out.println("No config file found, creating a new one.");
		// 	// createEmptyConfig(); // Creates file with global and default values.
		// }
		parseInput(); // Appends frontend-backend pairs to it.
		// haproxyReload();
		// formatFile();
	}




	public static void options(String args[]) throws IOException {

        System.out.println("Mini Haproxy config manipulater\n");
        if (args.length == 0) {
            System.out.println(String.format("Help window"));
            System.out.println(String.format("%-10s Print Config on Screen\n" + "", "dump"));
            System.exit(-1);
        }
        switch (args[0]) {
        case "dump":
            // dumpConfig();
            break;

        default:
            break;
        }

    }

	
	public static void parseInput() throws IOException, ParseException {

		JSONParser parser = new JSONParser();

		Object obj = parser.parse(new FileReader("input.json"));

		JSONObject jsonObject = (JSONObject) obj;

		for (Iterator iterator = jsonObject.keySet().iterator(); iterator.hasNext();) {
			ConfigFile configFile = new ConfigFile();
			Template template = new Template();
			String key = (String) iterator.next();
			configFile.setUniqueID(key);
			// System.out.println("The key is = "+key);
			// System.out.println(jsonObject.get(key));
			JSONObject jsonObject2 = (JSONObject) jsonObject.get(key);
			for (Iterator iterator2 = jsonObject2.keySet().iterator(); iterator2.hasNext();) {
				String key2 = (String) iterator2.next();
				String id;
				// configFile.set(key2) = jsonObject2.get(key2);
				// System.out.print("The key is ="+key2);
				// System.out.println(";The value is ="+jsonObject2.get(key2));
				if (key2.equalsIgnoreCase("action")) {
					configFile.setAction((String) jsonObject2.get(key2));
				}
				;

				if (key2.equalsIgnoreCase("mode")) {
					configFile.setMode((String) jsonObject2.get(key2));
				}
				;

				if (key2.equalsIgnoreCase("application")) {
					configFile.setApplication((String) jsonObject2.get(key2));
				}
				;

				if (key2.equalsIgnoreCase("client_IP")) {
					configFile.setClient_IP((String) jsonObject2.get(key2));
				}
				;

				if (key2.equalsIgnoreCase("server_IP")) {
					// We should store the String items in an array.
					List<String> serversList = new ArrayList<String>();
					JSONArray serversJson = (JSONArray) jsonObject2.get(key2);
					// System.out.println(serversJson);
					Iterator i = serversJson.iterator();
					while (i.hasNext()){
						serversList.add((String)i.next());
					}
					// System.out.println(serversList);
					configFile.setServer_IP(serversList);
				}
				;

				if (key2.equalsIgnoreCase("force_Server_PORT")) {
					configFile.setForce_Server_PORT((String) jsonObject2.get(key2));
				}
				;

				if (key2.equalsIgnoreCase("ssl")) {
					configFile.setSsl((boolean) jsonObject2.get(key2));
				}
				;

				if (key2.equalsIgnoreCase("cert_path")) {
					configFile.setCert_path((String) jsonObject2.get(key2));
				}
				;
			}
		
			HaproxyCore core = new HaproxyCore(configFile);
			core.run();
	
		}

		HaproxyFileManagement fileManagement = new HaproxyFileManagement(configName);

		boolean flag = fileManagement.doesConfigExist();
        if(flag)
            fileManagement.takeBackUp();



		HaproxyProcess haproxyProcess = new HaproxyProcess();

		if (haproxyProcess.running()) {
			System.out.println("Haproxy is already running, reloading configuration.");
			haproxyProcess.haproxyReload();
		} else {
			System.out.println("Haproxy is not currently running. Starting it...");
			haproxyProcess.haproxyStart();
		}

	}
}

