
// Java program to demonstrate positional access 
// operations on List interface 
import java.io.File;
import java.io.IOException;
import java.util.*;

public class playgroud {
    public static void main(String[] args) throws IOException {
        
        File f = new File("trial.txt");
        int i = 0;
        while (i < 6) {
            if (f.exists()) {
                f = new File("trial" + i);
                f.createNewFile();
                i++;
            }
        } // while
    }

}
