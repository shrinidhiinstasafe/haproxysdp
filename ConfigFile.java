import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;


public class ConfigFile {

	private String action;
	private String mode = "http"; //default mode "http"
    private String application;
    private String client_IP = "*:443"; //default 443
	private List<String> server_IP;
	private String force_Server_PORT = null;
	private boolean ssl = true; //default SSL
	private String cert_path;


	public ConfigFile() {
	}

	public ConfigFile(String action, String mode, String application, String client_IP, List<String> server_IP,
			String force_Server_PORT, boolean ssl, String cert_path) {
		this.action = action;
		this.mode = mode;
		this.application = application;
		this.client_IP = client_IP;
		this.server_IP = server_IP;
		this.force_Server_PORT = force_Server_PORT;
		this.ssl = ssl;
		this.cert_path = cert_path;
	}

	private String uniqueID;
	public void setUniqueID(String uniqueID){
		this.uniqueID = uniqueID;
	}

	public String getUniqueID(){
		return this.uniqueID;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getMode() {
		return this.mode;
	}

	public void setMode(String mode) {
		System.out.println("Mode is set");
		this.mode = mode;
	}

	public String getApplication() {
		return this.application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getClient_IP() {
		return this.client_IP;
	}

	public void setClient_IP(String client_IP) {
		this.client_IP = client_IP;
	}

	public List<String> getServer_IP() {
		return this.server_IP;
	}

	public void setServer_IP(List<String> serversList) {
		this.server_IP = serversList;
	}

	public String getForce_Server_PORT() {
		return this.force_Server_PORT;
	}

	public void setForce_Server_PORT(String force_Server_PORT) {
		this.force_Server_PORT = force_Server_PORT;
	}

	public boolean isSsl() {
		return this.ssl;
	}

	public boolean getSsl() {
		return this.ssl;
	}

	public void setSsl(boolean ssl) {
		this.ssl = ssl;
	}

	public String getCert_path() {
		return this.cert_path;
	}

	public void setCert_path(String cert_path) {
		this.cert_path = cert_path;
	}

	public ConfigFile action(String action) {
		this.action = action;
		return this;
	}

	public ConfigFile mode(String mode) {
		this.mode = mode;
		return this;
	}

	public ConfigFile application(String application) {
		this.application = application;
		return this;
	}

	public ConfigFile client_IP(String client_IP) {
		this.client_IP = client_IP;
		return this;
	}

	public ConfigFile force_Server_PORT(String force_Server_PORT) {
		this.force_Server_PORT = force_Server_PORT;
		return this;
	}

	public ConfigFile ssl(boolean ssl) {
		this.ssl = ssl;
		return this;
	}

	public ConfigFile cert_path(String cert_path) {
		this.cert_path = cert_path;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof ConfigFile)) {
			return false;
		}
		ConfigFile configFile = (ConfigFile) o;
		return Objects.equals(action, configFile.action) && Objects.equals(mode, configFile.mode)
				&& Objects.equals(application, configFile.application)
				&& Objects.equals(client_IP, configFile.client_IP) && Objects.equals(server_IP, configFile.server_IP)
				&& Objects.equals(force_Server_PORT, configFile.force_Server_PORT) && ssl == configFile.ssl
				&& Objects.equals(cert_path, configFile.cert_path);
	}

	@Override
	public int hashCode() {
		return Objects.hash(action, mode, application, client_IP, server_IP, force_Server_PORT, ssl, cert_path);
	}

	@Override
	public String toString() {
		return "{" + " action='" + getAction() + "'" + ", mode='" + getMode() + "'" + ", application='"
				+ getApplication() + "'" + ", client_IP='" + getClient_IP() + "'" + ", server_IP='" + getServer_IP()
				+ "'" + ", force_Server_PORT='" + getForce_Server_PORT() + "'" + ", ssl='" + isSsl() + "'"
				+ ", cert_path='" + getCert_path() + "'" + "}";
	}

}
