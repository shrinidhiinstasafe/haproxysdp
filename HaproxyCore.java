
// Creating a text File using FileWriter 
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
// Reading data from a file using FileReader 
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class HaproxyCore {

    String configPath = "/etc/haproxy/";
    String configExt = "cfg"; // cfg - production
    String configName = "haproxy." + configExt;

    ConfigFile configFile;

    public HaproxyCore(ConfigFile configFile, String configName) {
        this.configFile = configFile;
        this.configName = configName;
    }

    public HaproxyCore(ConfigFile configFile) {
        this.configFile = configFile;
    }

    public void run() throws IOException {

        Template template = new Template();
        template = fillUpTemplate(template, configFile);

        String key = configFile.getUniqueID();

        HaproxyFileManagement fileManagement = new HaproxyFileManagement(configName);

        if ((configFile.getAction()).equalsIgnoreCase("create"))
            fileManagement.createConfigFile(template, key);

        if ((configFile.getAction()).equalsIgnoreCase("DELETEAPP")) {
            fileManagement.deleteSegment("FRONT", key);
            fileManagement.deleteSegment("BACK", key);
        }
        if ((configFile.getAction()).equalsIgnoreCase("DELETESERVERS")) {
            for (String eachServer : configFile.getServer_IP()) {
                fileManagement.deleteSegment("SERVER" + eachServer, key);
            }
        }

        fileManagement.formatFile();
    }



    public Template fillUpTemplate(Template data, ConfigFile configFile) {
        
        try {

            // Filling up the frontend.
            String front = data.getFrontend();
            System.out.println("Frontend before replace = " + front);
            front = front.replace("{mode}", configFile.getMode());
            front = front.replace("{app}", configFile.getApplication());
            front = front.replace("{client_IP}", configFile.getClient_IP());
            if (configFile.getSsl())
                front = front.replace("{cert}", "crt " + configFile.getCert_path());
            else
                front = front.replace("{cert}", "");
            data.setFrontend(front);

            System.out.println("Data after the frontend is filled = " + front);
            // Filling up the backend.
            String back = data.getBackend();
            back = back.replace("{mode}", configFile.getMode());
            back = back.replace("{app}", configFile.getApplication());

            // back = back.replace("{server_IP}", configFile.getServer_IP());

            List<String> arrayList = new ArrayList<String>();
            arrayList = configFile.getServer_IP();
            int i = 0;
            for (String eachServer : arrayList) {
                if (configFile.getForce_Server_PORT() == null || configFile.getForce_Server_PORT().isEmpty()) {
                    back += "\n\t#BEGINSERVER" + eachServer + "-" + configFile.getUniqueID() + "\n";
                    back += ("\tserver " + configFile.getApplication() + "_server" + (i++) + " " + eachServer
                            + " check \n");
                    back += "\t#ENDSERVER" + eachServer + "-" + configFile.getUniqueID() + "\n\n";
                } else {
                    back += "\n\t#BEGINSERVER" + eachServer + "-" + configFile.getUniqueID() + "\n";
                    back += ("\tserver " + configFile.getApplication() + "_server" + (i++) + " " + eachServer
                            + " check " + "port " + configFile.getForce_Server_PORT() + "\n");
                    back += "\t#ENDSERVER" + eachServer + "-" + configFile.getUniqueID() + "\n\n";

                }

            }

            if (configFile.getForce_Server_PORT() == null || configFile.getForce_Server_PORT().isEmpty()) {
                back = back.replace("{server_PORT}", "");
            } else {
                back = back.replace("{server_PORT}", " port " + configFile.getForce_Server_PORT());
            }
            data.setBackend(back);
        } catch (Exception e) {
            // System.out.println(e);
        }

        return data;

    }

}

