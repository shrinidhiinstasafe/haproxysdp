import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class HaproxyProcess {

    String socketFile = "/var/run/haproxy.sock";
    String configName = "haproxy.cfg";
    String pidFile = "/var/run/haproxy.pid";

    public HaproxyProcess(){

    }

    public HaproxyProcess(String configName){
        this.configName = configName;
    }

    public HaproxyProcess(String configName, String socketFile,String pidFile){
        this.configName = configName;
        this.socketFile = socketFile;
        this.pidFile = pidFile;
    }

    public boolean running() throws IOException {
        // System.out.println("We are testing if haproxy is currently running or not.");
        ProcessBuilder builder = new ProcessBuilder( "bash", "-c" , "echo \"show stat\" | nc -U "+socketFile);
        System.out.println(builder.command());
        builder.redirectErrorStream(true);
        Process process = builder.start();
        InputStream is = process.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));

        String line = null;
        String output = null;
        while ((line = reader.readLine())!= null) {
            output += line;
        }
        
        System.out.println(output);
        System.out.println("The output has the length = "+output.length());
        if (output.length()<60){
            return false;
        }
        else return true;

    }

    public void haproxyStart() throws IOException {

        ProcessBuilder builder = new ProcessBuilder("bash", "-c", "haproxy  -D -f "+configName);
        builder.redirectErrorStream(true);
        System.out.println(builder.command());
        Process process = builder.start();
        InputStream is = process.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));

        String line = null;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }

    }


    public void haproxyReload() throws IOException {
        ProcessBuilder builder = new ProcessBuilder("bash", "-c", "haproxy -D -f "+configName+" -p "+pidFile+" -sf $(cat "+pidFile+")");
        builder.redirectErrorStream(true);
        System.out.println(builder.command());
        Process process = builder.start();
        InputStream is = process.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));

        String line = null;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }

    }
}