
// Creating a text File using FileWriter 
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
// Reading data from a file using FileReader 
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


import static java.nio.file.StandardCopyOption.*;


public class HaproxyFileManagement {

    String configName;

    public HaproxyFileManagement() {
    }

    public HaproxyFileManagement(String configName) {
        this.configName = configName;
    }

    // Delete a block of #BEGIN and #END

    public void deleteSegment(String slug, String uniqueID) {

        String lineToRemove = "#BEGIN" + slug + "-" + uniqueID;
        String removeUntil = "#END" + slug + "-" + uniqueID;

        try {
            File inputFile = new File(configName);
            File tempFile = new File("temp");

            BufferedReader reader = new BufferedReader(new FileReader(inputFile));
            BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

            String currentLine;

            boolean flag = false;

            while ((currentLine = reader.readLine()) != null) {
                // trim newline when comparing with lineToRemove

                String trimmedLine = currentLine.trim();
                if (trimmedLine.equals(lineToRemove)) {
                    flag = true;
                    continue;
                }
                if (flag) {
                    if (trimmedLine.equals(removeUntil)) {
                        flag = false;
                        continue;
                    } else
                        continue;
                }
                writer.write(currentLine + System.getProperty("line.separator"));
            }
            writer.close();
            reader.close();
            boolean successful = tempFile.renameTo(inputFile);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /// To make the end file look more presentable.
    public void formatFile() {
        System.out.println("Formating file now");
        try {
            File inputFile = new File(configName);
            File tempFile = new File("temp");

            BufferedReader reader = new BufferedReader(new FileReader(inputFile));
            BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

            String currentLine;

            boolean prev = false;
            int i = 0;
            while ((currentLine = reader.readLine()) != null) {
                // trim newline when comparing with lineToRemove
                if (currentLine.length() <= 2) {
                    if (prev)
                        continue;
                    prev = true;
                } else
                    prev = false;
                i = 0;
                writer.write(currentLine + System.getProperty("line.separator"));
            }
            writer.close();
            reader.close();
            boolean successful = tempFile.renameTo(inputFile);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    // Simply print the whole conf file on the screen
    public void dumpConfig() throws IOException {
        FileReader fr = null;
        int ch;
        try {
            fr = new FileReader(configName);
        } catch (FileNotFoundException fe) {
            System.out.println("File not found");
        }

        // read from FileReader till the end of file
        while ((ch = fr.read()) != -1)
            System.out.print((char) ch);

        // close the file
        fr.close();

    }

    // Append the template information to the end of the file surrounded by #Begin
    // and #End blocks.

    public void createConfigFile(Template obj, String id) {
        try {

            File file = new File(configName);
            FileWriter fr = new FileWriter(file, true);
            BufferedWriter br = new BufferedWriter(fr);
            br.write("\n#BEGINFRONT-" + id + "\n");
            br.write(obj.getFrontend());
            br.write("#ENDFRONT-" + id + "\n");
            br.write("\n#BEGINBACK-" + id + "\n");
            br.write(obj.getBackend());
            br.write("#ENDBACK-" + id + "\n");
            br.close();
            fr.close();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void createEmptyConfig() {
        try {
            Template data = new Template();
            PrintWriter file = new PrintWriter(configName);
            System.out.println(System.getProperty("user.dir"));
            file.write(data.getGlobal());
            file.write(data.getDefaults());
            file.close();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public boolean doesConfigExist() {

        File tempFile = new File(configName);
        boolean exists = tempFile.exists();
        return exists;
    }

	public void takeBackUp() throws IOException {

        String backupName = configName+".bak";
        Path configFile = Paths.get(configName);
        int i = 0;
        while (true){
            i = i + 1;

            File tempName = new File (backupName+i);
            if(tempName.exists())
                continue;
            else{
                Files.copy(configFile, Paths.get(backupName+i));
                break;
            }
        }

	}
}