public class Template {
    private String global = "" + "global\n" + "\tpidfile     /var/run/haproxy.pid\n"
            + "\tstats socket /var/run/haproxy.sock mode 600 level admin\n" + "\n";
    private String defaults = "" + "defaults \n" + "\ttimeout connect 10s \n" + "\ttimeout client 30s \n"
            + "\ttimeout server 30s \n" + "\tlog global \n" + "\tmode http \n" + "\toption httplog \n"
            + "\tmaxconn 3000\n" + "\n";
    private String frontend = "" + "frontend {mode}_{app}\n" + "\tbind {client_IP} {cert}\n" + "\tmode {mode}\n"
            + "\tdefault_backend {app}\n";

    private String backend = "" + "backend {app}\n" + "\tmode {mode}\n";

    // private String server = "\tserver {app}_server {server_IP} check
    // {server_PORT}\n";

    public Template() {
    }

    public Template(String global, String defaults, String frontend, String backend) {
        this.global = global;
        this.defaults = defaults;
        this.frontend = frontend;
        this.backend = backend;
    }

    public void setGlobal(String global) {
        this.global = global;
    }

    public void setDefaults(String defaults) {
        this.defaults = defaults;
    }

    public void setFrontend(String frontend) {
        this.frontend = frontend;
    }

    public void setBackend(String backend) {
        this.backend = backend;
    }

    public String getGlobal() {
        return global;
    }

    public String getDefaults() {
        return defaults;
    }

    public String getFrontend() {
        return frontend;
    }

    public String getBackend() {
        return backend;
    }
}
