#!/bin/sh
echo "Data Plane API is a sidecar process that runs next to HAProxy and provides 
API endpoints for managing HAProxy. It requires HAProxy version 1.9.0 or higher.

."
echo "Run this as root"
echo "Assuming that we are running this in Ubuntu environment"
if [ `id -u` != 0 ]; then
 echo "Root permission is needed to run this installer!"
 exit 1
fi


wget https://github.com/haproxytech/dataplaneapi/releases/download/v1.2.4/dataplaneapi
chmod +x dataplaneapi
sudo cp dataplaneapi /usr/local/bin/

cp conf /etc/systemd/system/haproxy.service 
systemctl start haproxy

