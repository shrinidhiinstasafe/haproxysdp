#!/bin/sh
echo "HAProxy Installer. HAProxy is well know for its performance as a reverse-proxy and load-balancer."
echo "Run this as root"
echo "Assuming that we are running this in Ubuntu environment"
if [ `id -u` != 0 ]; then
 echo "Root permission is needed to run this installer!"
 exit 1
fi



apt install libffi-dev  
apt install libldap2-dev


cd /var/www/
git clone https://github.com/Aidaho12/haproxy-wi.git /var/www/haproxy-wi
chown -R www-data:www-data haproxy-wi/
pip3 install -r haproxy-wi/requirements.txt 
chmod +x haproxy-wi/app/*.py  
sudo ln -s /usr/bin/python3.5 /usr/bin/python3
