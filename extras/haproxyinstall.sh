#!/bin/sh
echo "HAProxy Installer. HAProxy is well know for its performance as a reverse-proxy and load-balancer."
echo "Run this as root"
echo "Assuming that we are running this in Ubuntu environment"
if [ `id -u` != 0 ]; then
 echo "Root permission is needed to run this installer!"
 exit 1
fi

# To monitor curls
# while sleep 1; do curl -w "$(date +%FT%T) dns %{time_namelookup} connect %{time_connect} firstbyte %{time_starttransfer} total %{time_total} HTTP %{http_code}\n" -o /dev/null -s "http://134.209.154.215"; done 


# Required to use systemd in ubuntu.
apt install libsystemd-dev
apt install libssl-dev	
apt install libpcre3-dev

cd /usr/src/
wget http://www.haproxy.org/download/2.0/src/haproxy-2.0.13.tar.gz
tar xvf haproxy-2.0.13.tar.gz
cd haproxy-2.0.13/
make TARGET=linux-glibc USE_STATIC_PCRE=1 USE_OPENSSL=1 USE_SYSTEMD=1
make PREFIX=/opt/haproxy-ssl install
ln -s /opt/haproxy-ssl/sbin/haproxy /usr/bin/haproxy
ls -s /opt/haproxy-ssl/sbin/haproxy /usr/local/sbin/haproxy

