#!/bin/sh
echo "InstaSafe Installer for Linux"
echo "Usage: sudo sh InstaSafe-Agent.sh"
if [ `id -u` != 0 ]; then
 echo "Root permission is needed to run this installer!"
 exit 1
fi
echo "Setting up InstaSafe would require Internet connectivity"
echo "Identifying version of Linux..."
if [ $(grep -c Ubuntu /etc/issue) -ne 0 ] ; then
	 echo "Installing dependencies for Ubuntu"
	 apt-get install openvpn -y --force-yes || { echo "Could not install the dependencies required";  exit 1; } 
	 apt-get install wget -y --force-yes 
	 wget -O /etc/init.d/instasafe http://repo.instasafe.net/pub/instasafe_ui >/dev/null 2>&1
	 chmod 700 /etc/init.d/instasafe
	 update-rc.d instasafe defaults
elif [ $(grep -c Mint /etc/issue) -ne 0 ] ; then
	 echo "Installing dependencies for Ubuntu"
	 apt-get install openvpn -y --force-yes || { echo "Could not install the dependencies required";  exit 1; } 
	 apt-get install wget -y --force-yes 
	 wget -O /etc/init.d/instasafe http://repo.instasafe.net/pub/instasafe_u >/dev/null 2>&1
	 chmod 700 /etc/init.d/instasafe
	 update-rc.d instasafe defaults
elif [ $(grep -c Debian /etc/issue) -ne 0 ] ; then
	 echo "Installing dependencies for Debian"
	 apt-get update
	 apt-get install openvpn -y --force-yes || { echo "Could not install the dependencies required" ;exit 1; }
elif [ -s /etc/redhat-release ]; then
                yum -y install wget
                wget -O /etc/init.d/instasafe http://repo.instasafe.net/pub/instasafe_r >/dev/null 2>&1
	        chmod 700 /etc/init.d/instasafe
	        chkconfig --add instasafe
                chkconfig instasafe on
	if [ $(yum -y install openvpn | grep -c "No package openvpn") -ne 0 ]; then
	        echo "Trying to update the repositories..."
		arch=`uname -i`
		echo "Detected architecture is $arch"
		MAJOR=$(uname -r | grep -o 'el[0-9]')
            if [ $MAJOR == "el5" ]; then
                    rpm -Uvh http://repo.instasafe.net/pub/epel-release-5-4.noarch.rpm
                        sed -i 's/https/http/g' /etc/yum.repos.d/epel.repo
            elif [ $MAJOR == "el6" ]; then
                        rpm -Uvh http://repo.instasafe.net/pub/epel-release-6-8.noarch.rpm
                        sed -i 's/https/http/g' /etc/yum.repos.d/epel.repo
                elif [ $MAJOR == "el7" ]; then
                        rpm -Uvh http://repo.instasafe.net/pub/epel-release-7-9.noarch.rpm
                        sed -i 's/https/http/g' /etc/yum.repos.d/epel.repo
                fi
		echo "A new repository is installed"
		yum check-update 
		yum -y install openvpn || { echo "Could not install the dependencies required for InstaSafe" ;exit 1; }
	        echo "Installed the dependencies"
	fi
elif [ -s /etc/system-release ] && [ $(grep -c Amazon /etc/system-release) -ne 0 ] ; then
        echo " Installing Dependencies for Amazon Linux "
        yum -y install openvpn wget
        wget -O /etc/init.d/instasafe http://repo.instasafe.net/pub/instasafe_r >/dev/null 2>&1
        chmod 700 /etc/init.d/instasafe
        chkconfig --add instasafe
        chkconfig instasafe on
elif [ -s /etc/SuSE-release ] && [ $(grep -c SUSE /etc/SuSE-release) -ne 0 ] ; then
        echo " Installing Dependencies for Suze Linux "
        zypper -n in openvpn 
        zypper -n in wget
        wget -O /etc/init.d/instasafe http://repo.instasafe.net/pub/instasafe_s > /dev/null 2>&1
        chmod 700 /etc/init.d/instasafe
        chkconfig --add instasafe
        chkconfig instasafe on
fi
if [ ! -d /etc/instasafe/ ]; then
    mkdir -p /etc/instasafe/
fi
echo "Configuring InstaSafe..."
echo "
# Config file for admin
daemon
dev tun
keepalive 10 180
persist-key
nobind
client
comp-lzo
server-poll-timeout 30
#management 127.0.0.1 22222
#management-query-passwords
auth-retry nointeract
script-security 3
route-delay 30 30
reneg-sec 0
remote-cert-tls server
float
cipher aes-128-cbc
<cert>
-----BEGIN CERTIFICATE-----
MIIEqDCCA5CgAwIBAgICB6IwDQYJKoZIhvcNAQELBQAwgcAxCzAJBgNVBAYTAklO
MQswCQYDVQQIEwJLQTESMBAGA1UEBxMJQmFuZ2Fsb3JlMSkwJwYDVQQKEyBJbnN0
YVNhZmUgVGVjaG5vbG9naWVzIFB2dC4gTFRkLjEsMCoGA1UEAxMjSW5zdGFTYWZl
IFRlY2hub2xvZ2llcyBQdnQuIExUZC4gQ0ExFTATBgNVBCkTDEluc3RhU2FmZSBD
QTEgMB4GCSqGSIb3DQEJARYRb3BzQGluc3Rhc2FmZS5jb20wHhcNMTkwNTA2MTAx
NzUyWhcNMjkwNTAzMTAxNzUyWjB2MQswCQYDVQQGEwJJTjELMAkGA1UECBMCS0Ex
EjAQBgNVBAcTCUJhbmdhbG9yZTEMMAoGA1UEChMDbmV3MRIwEAYDVQQDFAlhZG1p
bkBuZXcxJDAiBgkqhkiG9w0BCQEWFXN1cHBvcnRAaW5zdGFzYWZlLmNvbTCBnzAN
BgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAlp53oH2amkfy82h6evO5TfSLnE5fOj7f
JArAYrsEdyPdKiaW8TGNcMBd8is27mWUIHZjJB28uV8dhQfE8opvLkQ7cdPWEkAT
c6mGIzX6YNqCPlQ7k13TlqbBAbYvVeV7mbogJr+LSY8KkL0OAmfYyNbduvKFvXQX
z5iBb2o420UCAwEAAaOCAXcwggFzMAkGA1UdEwQCMAAwLQYJYIZIAYb4QgENBCAW
HkVhc3ktUlNBIEdlbmVyYXRlZCBDZXJ0aWZpY2F0ZTAdBgNVHQ4EFgQUIRub97fv
AoYUH4Zhc8XsHyK0KVwwgfUGA1UdIwSB7TCB6oAUASrbCK2Sz4vpvHurQI8v/h12
m1ShgcakgcMwgcAxCzAJBgNVBAYTAklOMQswCQYDVQQIEwJLQTESMBAGA1UEBxMJ
QmFuZ2Fsb3JlMSkwJwYDVQQKEyBJbnN0YVNhZmUgVGVjaG5vbG9naWVzIFB2dC4g
TFRkLjEsMCoGA1UEAxMjSW5zdGFTYWZlIFRlY2hub2xvZ2llcyBQdnQuIExUZC4g
Q0ExFTATBgNVBCkTDEluc3RhU2FmZSBDQTEgMB4GCSqGSIb3DQEJARYRb3BzQGlu
c3Rhc2FmZS5jb22CCQCYf29hIRA49jATBgNVHSUEDDAKBggrBgEFBQcDAjALBgNV
HQ8EBAMCB4AwDQYJKoZIhvcNAQELBQADggEBADWeCTDTwVHW2yEmcQICn5nsBhqq
L5lI8DI6mQBswSx1QKCZHDyXbYhUbUXVTeK7gusU1oYa3uZRjfbR8sZGB3ws3PNn
6Uioce/LXRwUSXzvs5xkkXt8mKQPxi3tI+N+SLlrB/mmLTWQuNv1wYWQnPsyG9N5
iKp7L+yKSMeFaNew5f7CNX/JVWXj25aPDv0jM6g2CXgK4w0HeS5mpWUm/mdegZX2
hXgVngY3k4X7DGKWDe8MmzkhZXkl8m7SpC00IgenbQQXzd760CLEPx3KIzFXAIUW
3+l9uj+BWbaTNIODJ46gN8wubS7AjaDHafnY5XcV/uCA1jhxilMxvZA2Tw0=
-----END CERTIFICATE-----
</cert>
<key>
-----BEGIN PRIVATE KEY-----
MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJaed6B9mppH8vNo
enrzuU30i5xOXzo+3yQKwGK7BHcj3SomlvExjXDAXfIrNu5llCB2YyQdvLlfHYUH
xPKKby5EO3HT1hJAE3OphiM1+mDagj5UO5Nd05amwQG2L1Xle5m6ICa/i0mPCpC9
DgJn2MjW3bryhb10F8+YgW9qONtFAgMBAAECgYASRx67QiLN0CdidG5Ql10IYcHD
cRtLRS/s/sUD9i//iUGgiNhOYrYIVb/Q0H+1BGyqV9OGIXmygSFi8GNtD3GYWxKi
nYSkXXs6AZ7lC848pIenafX0DO/skY6VMd2y6G2DxQQk66zCmjqOo0HJy5N0hmjc
nooidxXm0mt/itCMYQJBAMgU2DqK0fEJ7gBm4ZPRugIB2alAozn+RdNlWbzWynnE
N2jhtpFzgM12wqFLae/D2eQ1MzShnyWQohAGZ3JDa30CQQDAtsC8Xy4V8mUGmjrB
LhPH+p36Fth8CEE7CMb1KirZb3ecBaBfezUa5aJWVKLKQKSYa3dw71HuVc2ArjaJ
QelpAkBc9ZpU8MSD+foTQ7mQxUNnxVZMldSAkEv+4LrlyuG921VnLF93irOQQppA
+Fna5SLqLXGCBYl8mYtCaMdNRle9AkEAgP+UIkPgGIbKA1Jn8/oA+cMTzl1OijAI
Yhjs/R0905Ce0wydHmzdzmRad0luMs50nyaIrEs2X8CFsBZtoRFCyQJAQwvmJ2+q
pjUv5nSernp1WBXIKPp3ChN5JVIafc6I2VA5m1sdg+JuGUvIjPFVHjXymsdx9kz7
cD9LEY1ADIEujg==
-----END PRIVATE KEY-----
</key>
<ca>
-----BEGIN CERTIFICATE-----
MIIFUjCCBDqgAwIBAgIJAJh/b2EhEDj2MA0GCSqGSIb3DQEBCwUAMIHAMQswCQYD
VQQGEwJJTjELMAkGA1UECBMCS0ExEjAQBgNVBAcTCUJhbmdhbG9yZTEpMCcGA1UE
ChMgSW5zdGFTYWZlIFRlY2hub2xvZ2llcyBQdnQuIExUZC4xLDAqBgNVBAMTI0lu
c3RhU2FmZSBUZWNobm9sb2dpZXMgUHZ0LiBMVGQuIENBMRUwEwYDVQQpEwxJbnN0
YVNhZmUgQ0ExIDAeBgkqhkiG9w0BCQEWEW9wc0BpbnN0YXNhZmUuY29tMB4XDTE3
MDkxNDA5MDEyMloXDTI3MDkxMjA5MDEyMlowgcAxCzAJBgNVBAYTAklOMQswCQYD
VQQIEwJLQTESMBAGA1UEBxMJQmFuZ2Fsb3JlMSkwJwYDVQQKEyBJbnN0YVNhZmUg
VGVjaG5vbG9naWVzIFB2dC4gTFRkLjEsMCoGA1UEAxMjSW5zdGFTYWZlIFRlY2hu
b2xvZ2llcyBQdnQuIExUZC4gQ0ExFTATBgNVBCkTDEluc3RhU2FmZSBDQTEgMB4G
CSqGSIb3DQEJARYRb3BzQGluc3Rhc2FmZS5jb20wggEiMA0GCSqGSIb3DQEBAQUA
A4IBDwAwggEKAoIBAQDess9gYKzTo+2HG0z8D0fUqaIc17Ju++v2km0ay4nkNkuS
AMeCkXgQYsLBbJJ+GEz8pyYGAhqAH4kI7kPhZyqqLO5xHPe70XteBGj9pusmwZOF
pYL8rh7vLZcAKsPPcbCtY9sZqN6rMs9LKpwBbBQSX/r2kVZZtUr1lOs+6zqv1SD/
6vTknDYT7axYmvaDd9KRzDmVzamLnmyjn20wRUSWGz8UvimPFV9IxX1f0COsxlcL
dyYcK2OT1hyfJ3pomzZUPirF37/gY9QB5J3q9XpJhBjFhO8xntBT/F7YDIc1MFAd
ouu+x++d+xWupDE59rv5t9Tu69x7Tji1/EyPUFlTAgMBAAGjggFLMIIBRzAdBgNV
HQ4EFgQUASrbCK2Sz4vpvHurQI8v/h12m1QwgfUGA1UdIwSB7TCB6oAUASrbCK2S
z4vpvHurQI8v/h12m1ShgcakgcMwgcAxCzAJBgNVBAYTAklOMQswCQYDVQQIEwJL
QTESMBAGA1UEBxMJQmFuZ2Fsb3JlMSkwJwYDVQQKEyBJbnN0YVNhZmUgVGVjaG5v
bG9naWVzIFB2dC4gTFRkLjEsMCoGA1UEAxMjSW5zdGFTYWZlIFRlY2hub2xvZ2ll
cyBQdnQuIExUZC4gQ0ExFTATBgNVBCkTDEluc3RhU2FmZSBDQTEgMB4GCSqGSIb3
DQEJARYRb3BzQGluc3Rhc2FmZS5jb22CCQCYf29hIRA49jAMBgNVHRMEBTADAQH/
MCAGA1UdEQQZMBeBFXN1cHBvcnRAaW5zdGFzYWZlLmNvbTANBgkqhkiG9w0BAQsF
AAOCAQEAl4+sQn/I03vdPQDNnKNWiXdv4B+x74exovoC1k3KVDzVtHodGYzm2+eN
zXYNYe+lziXb79d6NtZrQPGfAO0270VbbpdOxwQz9J/sedmycO5u3zYSF8USRT61
QkjI/OR/1HLpAADYXX3lMFHwkRDSuDN6sHTIMyc//8uFRv1mwBzC9e+4njitaphr
UFx6FKV/TQaHKxa5SQbvlSlXeImjHzN0DrY4SEcpHope8XMKYlHBWgrurfSRpOKD
dd4M68nYV/CChHC8vmO/lzsFHt1nsicbqbUAaq511Snjxy1IRLbVLMTE/PZ3oiGT
aLThcP/FYUuZA4OnWsMIA/Kh6fMRtw==
-----END CERTIFICATE-----
</ca>
<tls-auth>
#
# 2048 bit OpenVPN static key
#
-----BEGIN OpenVPN Static key V1-----
92ee68aeca88fb005690da2957688939
8d2e02ae611b3a0b8d7a1e3814a2b61e
3b4d3f145290f710c06999a194c26e84
88ec3bc3eb41e176d50117cedf786a04
48d1c2e6b9be189762ff6784e02764b6
db84d6870e7c96cff57ade311d360414
08427b3dc2be96519448fc833889dc7c
64583b1e7abdd747751e5abcb26981e2
f1fa0299167d6d7f1c2ba9aa299ae935
7b26c42c1f5401b14fca38f46524a882
3233e4361cceba527d406519f3eca767
7154d1d39af322c78b73f7fd0b858044
ce428611835d4ce67328d1bec3a3936e
2643d2b108a4cbbb3ef9a47118e835a2
b6eaf62c8d308103c5d468cc098a13e5
f6711df035f811e497637a5ee08f3ee0
-----END OpenVPN Static key V1-----
</tls-auth>


remote isademo.instasafe.com 1336 udp

log /var/log/instasafe-admin.log

" > /etc/instasafe/new.conf ||  echo "Could not configure InstaSafe properly"
echo "Configuration Done"
echo "Installation Complete."
echo "Starting InstaSafe Service..."
echo "This action may prompt for the username/password."
echo "The username/password would be the same as entered on myInstaSafe portal"
service instasafe restart || /etc/init.d/instasafe restart
